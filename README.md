# JMS Camel Client Demo
[![pipeline status](https://gitlab.com/lco-utils/jms-camel-demo/badges/master/pipeline.svg)](https://gitlab.com/lco-utils/jms-camel-demo/commits/master)

A Camel-backed implementation of a Java JMS topic client (for ActiveMQ).
