/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lco.jms.camel.demo;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

public class BlockingSubscriber implements AutoCloseable {
    private final String topic;

    final CamelContext ctx = new DefaultCamelContext();
    final ConsumerTemplate template;

    public BlockingSubscriber(final String brokerUrl, final String topic) {
        this.topic = topic;

        final ConnectionFactory factory = new ActiveMQConnectionFactory(brokerUrl);
        ctx.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(factory));

        template = ctx.createConsumerTemplate();

        ctx.start();
    }

    /**
     * This method will block the current thread until a message is received.
     *
     * @return
     */
    public String receive() {
        final Exchange exchange = template.receive("jms:topic:" + topic);
        return exchange.getMessage().getBody(String.class);
    }

    /**
     * This method will block the current thread until a message is received or the timeout is reached.
     *
     * @param timeout
     * @return
     */
    public String receive(final long timeout) {
        final Exchange exchange = template.receive("jms:topic:" + topic, timeout);
        return exchange.getMessage().getBody(String.class);
    }

    @Override
    public void close() throws Exception {
        ctx.close();
    }
}
