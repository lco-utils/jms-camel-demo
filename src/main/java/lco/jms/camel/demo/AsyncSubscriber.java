package lco.jms.camel.demo;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;
import java.util.function.Consumer;

public class AsyncSubscriber implements AutoCloseable {
    final CamelContext ctx = new DefaultCamelContext();

    public AsyncSubscriber(final String brokerUrl, final String topic, final Consumer<String> handler) throws Exception {

        final ConnectionFactory factory = new ActiveMQConnectionFactory(brokerUrl);
        ctx.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(factory));

        ctx.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                from("jms:topic:" + topic)
                        .process(e -> {
                            handler.accept(e.getMessage().getBody(String.class));
                        });
            }
        });

        ctx.start();
    }


    @Override
    public void close() throws Exception {
        ctx.close();
    }
}
