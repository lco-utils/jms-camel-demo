package lco.jms.camel.demo;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

public class Publisher implements AutoCloseable {
    private final String topic;

    private final CamelContext ctx = new DefaultCamelContext();
    private final ProducerTemplate template;

    public Publisher(final String brokerUrl, final String topic) {
        this.topic = topic;

        final ConnectionFactory factory = new ActiveMQConnectionFactory(brokerUrl);
        ctx.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(factory));

        template = ctx.createProducerTemplate();
        ctx.start();
    }

    public void send(final String message) {
        template.sendBody("jms:topic:" + topic, message);
    }

    @Override
    public void close() throws Exception {
        ctx.close();
    }
}
